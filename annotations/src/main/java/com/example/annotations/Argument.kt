package com.example.annotations

@Retention(AnnotationRetention.SOURCE)
@Target(AnnotationTarget.FIELD)
annotation class Argument

// difference between AnnotationTarget.PROPERTY and AnnotationTarget.FIELD  ??
// AnnotationTarget.PROPERTY doesn't work when annotating in .java