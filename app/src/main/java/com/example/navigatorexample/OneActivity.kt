package com.example.navigatorexample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.annotations.Activity
import com.example.annotations.Argument
import kotlinx.android.synthetic.main.activity_one.*

@Activity
class OneActivity : AppCompatActivity() {

    @Argument
    var userId_1: Int = 7

    @Argument
    var userId_2: Double = 7.0

    @Argument
    var userId_3: Boolean = true

    @Argument
    var userId_4: Long = 7L

    // @Argument
    // lateinit var userId: String
    // maybe can use by inject() or by arg()
    // take inspiration from nav component and koin

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_one)

        OneActivityBinder(this)

        one_activity_TextView.text = """$userId_1 , $userId_2 , $userId_3 , $userId_4"""
    }

}
