package com.example.navigatorexample;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.annotations.Activity;
import com.example.annotations.Argument;

@Activity
public class TwoActivity extends AppCompatActivity {

    @Argument
    float planId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two);
    }
}
