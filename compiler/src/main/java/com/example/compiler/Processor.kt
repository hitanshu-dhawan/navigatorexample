package com.example.compiler

import com.example.annotations.Activity
import com.example.annotations.Argument
import com.squareup.kotlinpoet.*
import java.io.IOException
import javax.annotation.processing.*
import javax.lang.model.SourceVersion
import javax.lang.model.element.Element
import javax.lang.model.element.ElementKind
import javax.lang.model.element.TypeElement
import javax.lang.model.util.ElementFilter
import javax.lang.model.util.Elements
import javax.tools.Diagnostic

class Processor : AbstractProcessor() {

    private lateinit var elements: Elements
    private lateinit var messager: Messager
    private lateinit var filer: Filer

    override fun init(processingEnvironment: ProcessingEnvironment) {
        super.init(processingEnvironment)

        elements = processingEnvironment.elementUtils
        messager = processingEnvironment.messager
        filer = processingEnvironment.filer
    }

    override fun process(annotations: Set<TypeElement>, roundEnvironment: RoundEnvironment): Boolean {

        for (element in roundEnvironment.getElementsAnnotatedWith(Activity::class.java)) {
            if (element.kind != ElementKind.CLASS) {
                messager.printMessage(Diagnostic.Kind.ERROR, element.simpleName.toString() + " is not a class", element)
                return true
            }

            generateNavigator(element)
            generateBinder(element)

        }

        return true
    }

    private fun generateNavigator(element: Element) {
        val packageName: String = elements.getPackageOf(element).qualifiedName.toString()
        val file = FileSpec.builder(packageName, "${element.simpleName}Navigator")

        val startActivityFunction = FunSpec.builder("start${element.simpleName}")
            .receiver(ClassName("android.content", "Context"))
            .addStatement("val intent = %T(this, ${element.simpleName}::class.java)", ClassName("android.content", "Intent"))
            .also {
                for (variableElement in ElementFilter.fieldsIn(element.enclosedElements)) {
                    if (variableElement.getAnnotation(Argument::class.java) != null) {
                        it.addParameter(variableElement.simpleName.toString(), variableElement.asType().asTypeName())
                        it.addStatement("""intent.putExtra("${variableElement.simpleName}", ${variableElement.simpleName})""")
                    }
                }
            }
            .addStatement("this.startActivity(intent)")
            .build()

        file.addFunction(startActivityFunction)

        try {
            file.build().writeTo(filer)
        } catch (e: IOException) {
            messager.printMessage(Diagnostic.Kind.ERROR, e.toString())
        }
    }

    private fun generateBinder(element: Element) {
        val packageName: String = elements.getPackageOf(element).qualifiedName.toString()
        val file = FileSpec.builder(packageName, "${element.simpleName}Binder")

        val binderConstructor = FunSpec.constructorBuilder()
            .addParameter("activity", element.asType().asTypeName())
            .also {
                for (variableElement in ElementFilter.fieldsIn(element.enclosedElements)) {
                    if (variableElement.getAnnotation(Argument::class.java) != null) {
                        it.addStatement("""activity.${variableElement.simpleName} = activity.intent.extras?.get("${variableElement.simpleName}") as %T""", variableElement.asType().asTypeName())
                    }
                }
            }
            .build()

        val binderClass = TypeSpec.classBuilder("${element.simpleName}Binder")
            .primaryConstructor(binderConstructor)
            .build()

        file.addType(binderClass)

        try {
            file.build().writeTo(filer)
        } catch (e: IOException) {
            messager.printMessage(Diagnostic.Kind.ERROR, e.toString())
        }
    }

    override fun getSupportedAnnotationTypes(): MutableSet<String> {
        return mutableSetOf(Activity::class.java.canonicalName)
    }

    override fun getSupportedSourceVersion(): SourceVersion {
        return SourceVersion.latestSupported()
    }

}